package com.nycschools.repository;

import androidx.lifecycle.MutableLiveData;

import com.nycschools.model.School;
import com.nycschools.model.Score;
import com.nycschools.network.RetrofitBuilder;
import com.nycschools.network.State;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApiRepository {
    private static ApiRepository instance;

    public static ApiRepository getInstance() {
        if (instance == null) {
            instance = new ApiRepository();
        }
        return instance;
    }

    public void getSchoolList(final MutableLiveData<List<School>> schoolsLiveData, final MutableLiveData<Integer> state) {
        state.setValue(State.LOADING);

        RetrofitBuilder.getInstance()
                .api()
                .getSchoolList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> state.setValue(State.ERROR))
                .doOnSuccess(schoolsLiveData::setValue)
                .subscribe();
    }

    public void fetchSchoolScoreDetails(String id, final MutableLiveData<Score> schoolScore, final MutableLiveData<Integer> state) {

        RetrofitBuilder.getInstance()
                .api()
                .getScores(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> state.setValue(State.ERROR))
                .doOnSuccess(scores -> {
                    if (scores != null && scores.size() > 0) {
                        schoolScore.setValue(scores.get(0));
                        state.setValue(State.SUCCESS);
                    } else {
                        state.setValue(State.ERROR);
                    }
                })
                .subscribe();
    }

}
