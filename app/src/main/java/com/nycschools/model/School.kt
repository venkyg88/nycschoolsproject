package com.nycschools.model

/*
    Kotlin data class to avoid boilerplate code with getters and setters
 */
data class School(val school_name:String, val dbn:String){
}