package com.nycschools.model

import com.google.gson.annotations.SerializedName

/*
    Kotlin data class to avoid boilerplate code unlike in Java with getters and setters, the response data
    is directly parsed into these values
 */
data class Score(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String,
    @SerializedName("school_name")
    val schoolName: String
)