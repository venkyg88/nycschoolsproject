package com.nycschools.adapter;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nycschools.R;
import com.nycschools.model.School;

public class SchoolViewHolder extends RecyclerView.ViewHolder {
    private final TextView schoolNameTextView;
    private School school;

    public SchoolViewHolder( View itemView) {
        super(itemView);
        schoolNameTextView = itemView.findViewById(R.id.schoolName);
    }

    public void bind( School school, View.OnClickListener clickListener) {
        schoolNameTextView.setText(school.getSchool_name());
        schoolNameTextView.setOnClickListener(clickListener);
    }
}
