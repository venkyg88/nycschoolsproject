package com.nycschools.viewModel;

public class Constant {

    public static String ERROR_MESSAGE = "No School score data available";
    public static String OK_BUTTON = "OK";
}
