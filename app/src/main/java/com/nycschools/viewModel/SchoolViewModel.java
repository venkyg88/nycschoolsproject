package com.nycschools.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.nycschools.model.School;
import com.nycschools.repository.ApiRepository;
import java.util.List;


public class SchoolViewModel extends ViewModel{

    public MutableLiveData<List<School>> schoolsMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Integer> viewState = new MutableLiveData<>(0);

    public void getSchoolList() {
        ApiRepository.getInstance().getSchoolList( schoolsMutableLiveData, viewState);
    }

}
